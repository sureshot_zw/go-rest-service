#!/usr/bin/env sh

# abort on errors
set -e
docker pull symm/vape
docker run --rm --net=host -t -v $PWD/Vapefile:/Vapefile symm/vape:latest http://localhost:8080