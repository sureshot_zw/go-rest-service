#!/usr/bin/env sh

# abort on errors
set -e

docker build --build-arg USER_ID=$(id -u) --build-arg GROUP_ID=$(id -g) -t go-rest-service .
docker run -it --rm -p 8080:8080 go-rest-service