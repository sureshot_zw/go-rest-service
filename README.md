# Go-Rest Service

The idea here is to implement an example of a REST API in Golang that allows the user to get details of files contained in a designated folder inside the container.

Some of the things I added to this project
* Dockerised application, all you need is contained in the project and ready to run.
* Logging to catter for basic logging outside of error responses sent back for requests.
* Unit test coverage of key functionality. 
* Random file generation which allows the user to generate random files for purposes of running the application.
* Smoke tester for use in testing the API's end points.
* Data sanitization to sanitize data coming through HTTP requests.
* Environmental value usage to bring in config needed by the application. 

## Get it running
There is the long boring way of doing it manually:

```
docker build --build-arg USER_ID=$(id -u) --build-arg GROUP_ID=$(id -g) -t go-rest-service .
docker run -it --rm -p 8080:8080 go-rest-service
```
The first command initializes and gets the docker container you need running while the second command allows you to connect into the container through your terminal. 
*Take Note* that the Dockerfile is configured to run the main executable for the project so if you want to play you may want to comment out line 21 in `Dockerfile`

The easy way of it is to run `sh run.sh` from within the root level of the folder. This will run the 2 commands above for you and get you straight into the container. 

You will be asked how many files you want to generate. If you press `enter` it will go with the default of 100 files but if you enter a different value it will generate the files you request. 

The files are placed in the `test_files` folder which is created on the container for this purpose. 

Once files are created HTTP requests will now be receivable. 

Unit tests are run as part of the DockerFile build process and some will generate their own random files to fulfil their own unit tests. 

## HTTP Requests

To make a request you can use Insomnia Rest Client [Insomnia Rest Client](https://insomnia.rest/) or your browser though the former is better for this process.

Submit this url `http://localhost:8080/get-files?start=1&end=8&type=json` as the application is bound to port `8080` on `localhost`. 

The parameters of the request are as follows:
* `start` - index in the list of files to fetch. In a list of 100 files, a start can be 30
* `end` - index in the list to end fetch. In a list of 100 files, a end can be 40. This means 11 files will be returned in total
* `type` - type of response expected i.e json or xml

### Payload
An example of a json payload provided by the application is as follows:
```
{
  "Folder": "/go/src/service/test_files",
  "Files": [
    {
      "Name": "-c8bhihk-6",
      "Path": "/go/src/service/test_files/-c8bhihk-6",
      "Size": "22KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    },
    {
      "Name": "-fxi-1",
      "Path": "/go/src/service/test_files/-fxi-1",
      "Size": "2093KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    },
    {
      "Name": "-tfhsl5rf0l1",
      "Path": "/go/src/service/test_files/-tfhsl5rf0l1",
      "Size": "717KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    },
    {
      "Name": "0-pmjk",
      "Path": "/go/src/service/test_files/0-pmjk",
      "Size": "436KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    },
    {
      "Name": "051iwn8c2uc",
      "Path": "/go/src/service/test_files/051iwn8c2uc",
      "Size": "869KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    },
    {
      "Name": "06xd7jw0d78",
      "Path": "/go/src/service/test_files/06xd7jw0d78",
      "Size": "3925KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    },
    {
      "Name": "07li0a-20a3",
      "Path": "/go/src/service/test_files/07li0a-20a3",
      "Size": "804KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    },
    {
      "Name": "093jnyvbh0ge",
      "Path": "/go/src/service/test_files/093jnyvbh0ge",
      "Size": "3882KB",
      "Modified": "2021-02-20T13:52:08Z",
      "Permission": "-rw-r--r--"
    }
  ]
}
```

XML example response:
```
<Folder>
  <Folder>/go/src/service/test_files</Folder>
  <Files>
    <Name>-c8bhihk-6</Name>
    <Path>/go/src/service/test_files/-c8bhihk-6</Path>
    <Size>22KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
  <Files>
    <Name>-fxi-1</Name>
    <Path>/go/src/service/test_files/-fxi-1</Path>
    <Size>2093KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
  <Files>
    <Name>-tfhsl5rf0l1</Name>
    <Path>/go/src/service/test_files/-tfhsl5rf0l1</Path>
    <Size>717KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
  <Files>
    <Name>0-pmjk</Name>
    <Path>/go/src/service/test_files/0-pmjk</Path>
    <Size>436KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
  <Files>
    <Name>051iwn8c2uc</Name>
    <Path>/go/src/service/test_files/051iwn8c2uc</Path>
    <Size>869KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
  <Files>
    <Name>06xd7jw0d78</Name>
    <Path>/go/src/service/test_files/06xd7jw0d78</Path>
    <Size>3925KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
  <Files>
    <Name>07li0a-20a3</Name>
    <Path>/go/src/service/test_files/07li0a-20a3</Path>
    <Size>804KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
  <Files>
    <Name>093jnyvbh0ge</Name>
    <Path>/go/src/service/test_files/093jnyvbh0ge</Path>
    <Size>3882KB</Size>
    <Modified>2021-02-20T13:52:08Z</Modified>
    <Permission>-rw-r--r--</Permission>
  </Files>
</Folder>
```

## Smoke testing  
As part of monitoring and health checks a smoke tester was added to the project. You should run this when the HTTP requests are now receivable as the API's end points will be tested. To run it simply run `sh smoke-test.sh` in the root folder of the project. This will bring in a seperate docker image configured for smoke testing which works with the `Vapefile` for testing the api routes. Example output of a smoke test:

```
russell@russell-Inspiron-5566:~/workspace/go-rest-service$ sh smoke-test.sh 
Using default tag: latest
latest: Pulling from symm/vape
Digest: sha256:e7b4ebccecfea5ab77ed7508bb9b30aa47db4a127c784efeca61784d94b67e9d
Status: Image is up to date for symm/vape:latest
docker.io/symm/vape:latest
✓ [200:200] get-files?start=66&end=69&type=json
✓ [500:500] get-files?start=66&end=69&type=jack
✓ [200:200] get-files?start=66&end=69&type=xml

✨  [3/3] tests passed in 4.483397ms
```

Ideally this tool is run periodically in a production enviroment e.g. every 20 minutes to check for issues. With further expansion it can even be made to email someone in the event of a failed test. 

### Things I would love to add
* Authentication.
* Smoke testing automation.
* Request for a specific file. 