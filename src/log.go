package main

import (
    "os"
    "log"
    "time"
)

/*
 Generate random files to be used by the application
 */
func prepareLog() {
    if _, err := os.Stat("logs"); err != nil {
        if os.IsNotExist(err) {
            //Create a folder/directory at a full qualified path
            err := os.Mkdir("logs", 0755)
            if err != nil {
                log.Fatal(err.Error())
            }
        }
    }

    // If the file doesn't exist, create it or append to the file
    file, err := os.OpenFile("logs/log-" + time.Now().Format("2006-01-02") + ".txt", 
                              os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0775)
    if err != nil {
        log.Fatal(err.Error())
    }

    log.SetOutput(file)

    log.Println("Log file created")
}