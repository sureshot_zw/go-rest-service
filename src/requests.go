package main

import (
    "log"
    "net/http"
    "encoding/xml"
    "encoding/json"

    "github.com/go-sanitize/sanitize"
    "github.com/kelseyhightower/envconfig"
)

type Config struct {
    Name     string
    Port     string
}

type Filters struct {
    Start string    `san:"trim,xss"`
    End   string    `san:"trim,xss"`
    Type  string    `san:"trim,lower,xss"`
}

// get Files and return response as json
func getFiles(w http.ResponseWriter, r *http.Request){
    f := Filters{
        Start: r.URL.Query().Get("start"),
        End: r.URL.Query().Get("end"),
        Type: r.URL.Query().Get("type"),
    }

    s, err := sanitize.New()

    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(err.Error()))

        return
    }

    s.Sanitize(&f)

    // get this working
    results, limitfailure := getFolderInfo(f)

    if(limitfailure == true) {
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte("Incorrect Start and End range set"))

        return
    }

    if(f.Type == "json") {
        respondWithJson(results, w)

        return
    }

    if(f.Type == "xml") {
        respondWithXml(results, w)

        return
    }

    w.WriteHeader(http.StatusInternalServerError)
    w.Write([]byte("500 - Unknown response type requested"))
}

// Respond with json
func respondWithJson(results *Folder, w http.ResponseWriter) {
    js, err := json.Marshal(results)

    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(err.Error()))

        return
    }

    w.Header().Set("Content-Type", "application/json")
    w.Write(js)
}

// Respond with XMl
func respondWithXml(results *Folder, w http.ResponseWriter) {
    xml, err := xml.MarshalIndent(results, "", "  ")
    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(err.Error()))

        return
    }

    w.Header().Set("Content-Type", "application/xml")
    w.Write(xml)
}

// setup listener to listen in for incoming requests
func handleRequests() {
    var conf Config
    err := envconfig.Process("app", &conf)

    if err != nil {
        log.Fatal(err.Error())
    }

    http.HandleFunc("/get-files", getFiles)
    log.Fatal(http.ListenAndServe(":" + conf.Port, nil))
}