package main

func main() {
	// prepare log file
	prepareLog()
	
    // generate random files
    generateFiles(false)

    // handle requests
    handleRequests()
}