package main

import (
    "os"
    "testing"
    "io/ioutil"
)

// test to ensure random file generation is working
func TestFileInfo(t *testing.T) {
    generateFiles(true)

    path,_ := os.Getwd()
    testFilesPath := path + "/test_files"
    files,_ := ioutil.ReadDir(testFilesPath)

    if(len(files) != 100){
        t.Error("Failed to generate random files")
    }

    f := Filters{
        Start: "10",
        End: "20",
        Type: "json",
    }

    folder,_ := getFolderInfo(f)

    if(len(folder.Files) != 11){
        t.Error("Random files generated do not match")
    }
}