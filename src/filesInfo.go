package main

import (
    "os"
    "log"
    "time"
    "strconv"
    "io/ioutil"
)

// File details struct
type File struct {
    Name     string
    Path     string
    Size     string
    Modified time.Time
    Permission string
}


// Folder struct with additional files
type Folder struct {
    Folder   string
    Files    []*File
}

// Get folder 
// 
// @param Filters filters
// 
// @return []byte, boolean
func getFolderInfo(filters Filters) (*Folder, bool){
    start, err := strconv.Atoi(filters.Start)
    end, err   := strconv.Atoi(filters.End)
    files, err := ioutil.ReadDir("test_files")
    path, err := os.Getwd()
    testFilesPath := path + "/test_files"

    var randomFiles []*File

    if err != nil {
        log.Fatal(err.Error())
    }

    if err != nil {
        log.Fatal(err.Error())
    }

    limitFailure := false
    fileCount    := len(files)

    // if no start and end limit is set, we go with the first half of the files
    if(len(filters.Start) == 0 && len(filters.End) == 0) {
        start = 0
        end = fileCount / 2
    }

    // if start and end range is not sensible a limit failure response will be instigated
    if(end > fileCount || end < 0) {
        limitFailure = true
    }
 
    if(start > fileCount || start < 0) {
        limitFailure = true
    }
    
    if(limitFailure == false) {
        for i := start; i <= end; i++ { 
            if files[i].IsDir() == false {
                randomFiles = append(randomFiles, &File{
                                    Name : files[i].Name(), 
                                    Path : path + "/test_files/" + files[i].Name(), 
                                    Size :  strconv.FormatInt(files[i].Size(), 10) + "KB",
                                    Modified : files[i].ModTime(),
                                    Permission: files[i].Mode().String()})
            }
        } 

        fd := &Folder{Folder: testFilesPath, Files: randomFiles}

        return fd, false
    }
    
    return &Folder{Folder: testFilesPath}, true
}