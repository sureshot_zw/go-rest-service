package main

import (
    "os"
    "time"
    "testing"
)

// test to ensure logs are created at correct path
func TestPrepareLog(t *testing.T) {
    file := "logs/log-" + time.Now().Format("2006-01-02") + ".txt"
    
    prepareLog()

    if _, err := os.Stat(file); err != nil {
        if os.IsNotExist(err) {
            t.Error("Log file `" + file + "` does not exist") 
        }
    }
}