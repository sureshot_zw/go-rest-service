package main

import (
    "os"
    "log"
    "fmt"
    "strconv"
    "os/exec"
)

/*
 Generate random files to be used by the application

 @param boolean ignore input
 */
func generateFiles(ignoreInput bool) {
    fmt.Println("Please enter the number of files you want auto generated [Default: 100]: ")

    files := "100" 

    // Taking input from user 
    if(ignoreInput == false) {
        fmt.Scanln(&files)  

        // default to 100 files if no valid input is received
        if(files == "" ||isNumeric(files) == false) {
            files = "100"
        }
    }

    log.Println("Generating `" + files + "` files")

    // call cli command for generating files
    cmd := exec.Command(
        "random-files", 
        "-depth=0", 
        "-random-size=true", 
        "-files=" + files, 
        "-alphabet=easy",
        "-q=true",
        "test_files")
    
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stdout

    // run command
    if err := cmd.Run(); err != nil {
        log.Fatal(err.Error())
    }
}

// Check if string value is numeric 
// 
// @param string s
// 
// @return boolean
func isNumeric(s string) bool {
    _, err := strconv.ParseFloat(s, 64)
    return err == nil
}