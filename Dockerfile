FROM golang:latest
    ENV APP_USER app
    RUN unset GO111MODULE
    ENV GOPATH=/go
    ENV APP_HOME $GOPATH/src/service
    ENV APP_NAME "go-rest-service"
    ENV APP_PORT 8080
    ARG GROUP_ID
    ARG USER_ID
    RUN groupadd --gid $GROUP_ID app && useradd -m -l --uid $USER_ID --gid $GROUP_ID $APP_USER
    RUN mkdir -p $APP_HOME && chown -R $APP_USER:$APP_USER $APP_HOME
    USER $APP_USER
    ADD ./src $APP_HOME
    RUN go get -u github.com/jbenet/go-random-files/random-files
    RUN go get -u github.com/kelseyhightower/envconfig
    RUN go get -u github.com/go-sanitize/sanitize
    WORKDIR $APP_HOME
    RUN go build -o main
    EXPOSE 8080
    RUN go test
    RUN rm -rf test_files
    CMD ["./main"]